package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.entity.Flower;
import com.epam.rd.java.basic.task8.entity.GrowingTips;
import com.epam.rd.java.basic.task8.entity.VisualParameters;
import com.epam.rd.java.basic.task8.xmlTags.Tags;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for DOM parser.
 */
public class DOMController {

	private final String xmlFileName;

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public List<Flower> parse() {
		List<Flower> flowerList = new ArrayList<>();

		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder documentBuilder = dbf.newDocumentBuilder();
			Document doc = documentBuilder.parse(new File(xmlFileName));
			doc.getDocumentElement().normalize();

			NodeList nodeList = doc.getElementsByTagName(Tags.FLOWER);

			for (int nodeIndex = 0; nodeIndex < nodeList.getLength(); nodeIndex++) {
				Node node = nodeList.item(nodeIndex);

				if (node.getNodeType() == Node.ELEMENT_NODE) {
					Flower flower = new Flower();
					Element flowerDomEl = (Element) node;

					flower.setName(flowerDomEl.getElementsByTagName(Tags.NAME).item(0).getTextContent());
					flower.setSoil(flowerDomEl.getElementsByTagName(Tags.SOIL).item(0).getTextContent());
					flower.setOrigin(flowerDomEl.getElementsByTagName(Tags.ORIGIN).item(0).getTextContent());

					Element visualParametersDomEl = (Element) flowerDomEl.getElementsByTagName(Tags.VISUAL_PARAMETERS)
							.item(0);
					flower.setVisualParameters(parseVisualParameters(visualParametersDomEl));

					Element growingTipsDomEl = (Element) flowerDomEl.getElementsByTagName(Tags.GROWING_TIPS)
							.item(0);
					flower.setGrowingTips(parseGrowingTips(growingTipsDomEl));

					flower.setMultiplying(flowerDomEl.getElementsByTagName(Tags.MULTIPLYING)
							.item(0).getTextContent());

					flowerList.add(flower);
				}
			}

		} catch (ParserConfigurationException | IOException | SAXException e) {
			e.printStackTrace();
		}
		return flowerList;
	}

	private VisualParameters parseVisualParameters(Element domEl) {
		VisualParameters visualParameters = new VisualParameters();

		visualParameters.setStemColour(domEl.getElementsByTagName(Tags.STEM_COLOUR).item(0).getTextContent());
		visualParameters.setLeafColour(domEl.getElementsByTagName(Tags.LEAF_COLOUR).item(0).getTextContent());

		Element aveLenFlower = (Element) domEl.getElementsByTagName(Tags.AVE_LEN_FLOWER).item(0);
		visualParameters.setAveLenFlowerType(aveLenFlower.getAttribute(Tags.MEASURE));
		visualParameters.setAveLenFlowerValue(Integer.parseInt(aveLenFlower.getTextContent()));

		return visualParameters;
	}

	private GrowingTips parseGrowingTips(Element domEl) {
		GrowingTips growingTips = new GrowingTips();

		Element temperature = (Element) domEl.getElementsByTagName(Tags.TEMPERATURE).item(0);
		growingTips.setTemperatureType(temperature.getAttribute(Tags.MEASURE));
		growingTips.setTemperatureValue(Integer.parseInt(temperature.getTextContent()));

		Element lighting = (Element) domEl.getElementsByTagName(Tags.LIGHTING).item(0);
		growingTips.setLightRequiring(lighting.getAttribute(Tags.LIGHT_REQUIRING));

		Element watering = (Element) domEl.getElementsByTagName(Tags.WATERING).item(0);
		growingTips.setWateringMeasure(watering.getAttribute(Tags.MEASURE));
		growingTips.setWateringValue(Integer.parseInt(watering.getTextContent()));

		return growingTips;
	}

}
