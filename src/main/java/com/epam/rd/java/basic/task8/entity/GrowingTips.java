package com.epam.rd.java.basic.task8.entity;

public class GrowingTips {

    private String temperatureType;
    private int temperatureValue;
    private String lightRequiring;
    private String wateringMeasure;
    private int wateringValue;

    public GrowingTips(String temperatureType, int temperatureValue, String lightRequiring, String wateringMeasure, int wateringValue) {
        this.temperatureType = temperatureType;
        this.temperatureValue = temperatureValue;
        this.lightRequiring = lightRequiring;
        this.wateringMeasure = wateringMeasure;
        this.wateringValue = wateringValue;
    }

    public GrowingTips() {

    }

    public String getTemperatureType() {
        return temperatureType;
    }

    public void setTemperatureType(String temperatureType) {
        this.temperatureType = temperatureType;
    }

    public int getTemperatureValue() {
        return temperatureValue;
    }

    public void setTemperatureValue(int temperatureValue) {
        this.temperatureValue = temperatureValue;
    }

    public String isLightRequiring() {
        return lightRequiring;
    }

    public void setLightRequiring(String lightRequiring) {
        this.lightRequiring = lightRequiring;
    }

    public String getWateringMeasure() {
        return wateringMeasure;
    }

    public void setWateringMeasure(String wateringMeasure) {
        this.wateringMeasure = wateringMeasure;
    }

    public int getWateringValue() {
        return wateringValue;
    }

    public void setWateringValue(int wateringValue) {
        this.wateringValue = wateringValue;
    }

    @Override
    public String toString() {
        return "GrowingTips{" +
                "temperatureType='" + temperatureType + '\'' +
                ", temperatureValue=" + temperatureValue +
                ", lightRequiring=" + lightRequiring +
                ", wateringMeasure='" + wateringMeasure + '\'' +
                ", wateringValue=" + wateringValue +
                '}';
    }
}
