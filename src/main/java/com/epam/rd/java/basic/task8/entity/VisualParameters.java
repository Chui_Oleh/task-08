package com.epam.rd.java.basic.task8.entity;

public class VisualParameters {

    private String stemColour;
    private String leafColour;
    private String aveLenFlowerMeasure;
    private int aveLenFlowerValue;

    public VisualParameters(String stemColour, String leafColour, String aveLenFlowerType, int aveLenFlowerValue) {
        this.stemColour = stemColour;
        this.leafColour = leafColour;
        this.aveLenFlowerMeasure = aveLenFlowerType;
        this.aveLenFlowerValue = aveLenFlowerValue;
    }

    public VisualParameters() {
    }

    public String getStemColour() {
        return stemColour;
    }

    public void setStemColour(String stemColour) {
        this.stemColour = stemColour;
    }

    public String getLeafColour() {
        return leafColour;
    }

    public void setLeafColour(String leafColour) {
        this.leafColour = leafColour;
    }

    public String getAveLenFlowerType() {
        return aveLenFlowerMeasure;
    }

    public void setAveLenFlowerType(String aveLenFlowerType) {
        this.aveLenFlowerMeasure = aveLenFlowerType;
    }

    public int getAveLenFlowerValue() {
        return aveLenFlowerValue;
    }

    public void setAveLenFlowerValue(int aveLenFlowerValue) {
        this.aveLenFlowerValue = aveLenFlowerValue;
    }

    @Override
    public String toString() {
        return "VisualParameters{" +
                "stemColour='" + stemColour + '\'' +
                ", leafColour='" + leafColour + '\'' +
                ", aveLenFlowerMeasure='" + aveLenFlowerMeasure + '\'' +
                ", aveLenFlowerValue=" + aveLenFlowerValue +
                '}';
    }
}
