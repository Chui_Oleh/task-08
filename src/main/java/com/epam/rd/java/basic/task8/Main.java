package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;
import com.epam.rd.java.basic.task8.entity.Flower;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.util.List;

public class Main {

	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}

		XMLWriter xmlWriter = new XMLWriter();
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		
		DOMController domController = new DOMController(xmlFileName);
		List<Flower> domParsingResult = domController.parse();

		String outputXmlFile = "output.dom.xml";
		xmlWriter.write(domParsingResult, outputXmlFile);

		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		SAXController saxController = new SAXController(xmlFileName);

		SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
		SAXParser saxParser = saxParserFactory.newSAXParser();
		saxParser.parse(saxController.getXmlFileName(), saxController);

		List<Flower> saxParsingResult = saxController.getParsingResult();

		outputXmlFile = "output.sax.xml";
		xmlWriter.write(saxParsingResult, outputXmlFile);

		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		STAXController staxController = new STAXController(xmlFileName);
		List<Flower> staxParsingResult = staxController.parse();
		
		outputXmlFile = "output.stax.xml";
		xmlWriter.write(staxParsingResult, outputXmlFile);
	}

}
