package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.entity.Flower;
import com.epam.rd.java.basic.task8.entity.GrowingTips;
import com.epam.rd.java.basic.task8.entity.VisualParameters;
import com.epam.rd.java.basic.task8.xmlTags.Tags;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.util.List;

public class XMLWriter {

    public void write(List<Flower> flowerList, String filePath) {

        try {
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.newDocument();

            Element root = document.createElement(Tags.FLOWERS);
            root.setAttribute("xmlns", "http://www.nure.ua");
            root.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            root.setAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd ");
            document.appendChild(root);

            flowerList.forEach(flower -> {
                Element flowerEl = document.createElement(Tags.FLOWER);

                Element name = document.createElement(Tags.NAME);
                name.appendChild(document.createTextNode(flower.getName()));
                flowerEl.appendChild(name);

                Element soil = document.createElement(Tags.SOIL);
                soil.appendChild(document.createTextNode(flower.getSoil()));
                flowerEl.appendChild(soil);

                Element origin = document.createElement(Tags.ORIGIN);
                origin.appendChild(document.createTextNode(flower.getOrigin()));
                flowerEl.appendChild(origin);

                Element visualParameters = buildVisualParametersElement(flower.getVisualParameters(), document);
                flowerEl.appendChild(visualParameters);

                Element growingTips = buildGrowingTips(flower.getGrowingTips(), document);
                flowerEl.appendChild(growingTips);

                Element multiplying = document.createElement(Tags.MULTIPLYING);
                multiplying.appendChild(document.createTextNode(flower.getMultiplying()));
                flowerEl.appendChild(multiplying);

                root.appendChild(flowerEl);
            });

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource domSource = new DOMSource(document);
            StreamResult streamResult = new StreamResult(new File(filePath));

            transformer.transform(domSource, streamResult);

        } catch (ParserConfigurationException | TransformerException e) {
            e.printStackTrace();
        }

    }

    private Element buildVisualParametersElement(VisualParameters data, Document document) {
        Element root = document.createElement(Tags.VISUAL_PARAMETERS);

        Element stemColour = document.createElement(Tags.STEM_COLOUR);
        stemColour.appendChild(document.createTextNode(data.getStemColour()));
        root.appendChild(stemColour);

        Element leafColour = document.createElement(Tags.LEAF_COLOUR);
        leafColour.appendChild(document.createTextNode(data.getLeafColour()));
        root.appendChild(leafColour);

        Element aveLenFlower = document.createElement(Tags.AVE_LEN_FLOWER);
        aveLenFlower.setAttribute(Tags.MEASURE, data.getAveLenFlowerType());
        aveLenFlower.appendChild(document.createTextNode(String.valueOf(data.getAveLenFlowerValue())));
        root.appendChild(aveLenFlower);

        return root;
    }

    private Element buildGrowingTips(GrowingTips data, Document document) {
        Element root = document.createElement(Tags.GROWING_TIPS);

        Element temperature = document.createElement(Tags.TEMPERATURE);
        temperature.setAttribute(Tags.MEASURE, data.getTemperatureType());
        temperature.appendChild(document.createTextNode(String.valueOf(data.getTemperatureValue())));
        root.appendChild(temperature);

        Element lighting = document.createElement(Tags.LIGHTING);
        lighting.setAttribute(Tags.LIGHT_REQUIRING, data.isLightRequiring());
        root.appendChild(lighting);

        Element watering = document.createElement(Tags.WATERING);
        watering.setAttribute(Tags.MEASURE, data.getWateringMeasure());
        watering.appendChild(document.createTextNode(String.valueOf(data.getWateringValue())));
        root.appendChild(watering);

        return root;
    }

}
