package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entity.Flower;
import com.epam.rd.java.basic.task8.entity.GrowingTips;
import com.epam.rd.java.basic.task8.entity.VisualParameters;
import com.epam.rd.java.basic.task8.xmlTags.Tags;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

	private final String xmlFileName;

	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	// PLACE YOUR CODE HERE

	public List<Flower> parse() {
		List<Flower> flowerList = new ArrayList<>();
		Flower activeFlower = new Flower();

		XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
		try {
			XMLEventReader xmlEventReader = xmlInputFactory.createXMLEventReader(new FileReader(xmlFileName));

			while (xmlEventReader.hasNext()) {
				XMLEvent xmlEvent = xmlEventReader.nextEvent();

				if (xmlEvent.isStartElement()) {
					StartElement startElement = xmlEvent.asStartElement();
					String elementData;

					switch (startElement.getName().getLocalPart()) {
						case Tags.FLOWER:
							activeFlower = new Flower();
							break;
						case Tags.NAME:
							xmlEvent = xmlEventReader.nextEvent();
							elementData = xmlEvent.asCharacters().getData();
							activeFlower.setName(elementData);
							break;
						case Tags.SOIL:
							xmlEvent = xmlEventReader.nextEvent();
							elementData = xmlEvent.asCharacters().getData();
							activeFlower.setSoil(elementData);
							break;
						case Tags.ORIGIN:
							xmlEvent = xmlEventReader.nextEvent();
							elementData = xmlEvent.asCharacters().getData();
							activeFlower.setOrigin(elementData);
							break;
						case Tags.VISUAL_PARAMETERS:
							activeFlower.setVisualParameters(new VisualParameters());
							break;
						case Tags.STEM_COLOUR:
							xmlEvent = xmlEventReader.nextEvent();
							elementData = xmlEvent.asCharacters().getData();
							activeFlower.getVisualParameters().setStemColour(elementData);
							break;
						case Tags.LEAF_COLOUR:
							xmlEvent = xmlEventReader.nextEvent();
							elementData = xmlEvent.asCharacters().getData();
							activeFlower.getVisualParameters().setLeafColour(elementData);
							break;
						case Tags.AVE_LEN_FLOWER:
							String aveLenFlowerType = startElement.getAttributeByName(QName.valueOf(Tags.MEASURE))
									.getValue();
							xmlEvent = xmlEventReader.nextEvent();
							elementData = xmlEvent.asCharacters().getData();
							activeFlower.getVisualParameters().setAveLenFlowerType(aveLenFlowerType);
							activeFlower.getVisualParameters().setAveLenFlowerValue(Integer.parseInt(elementData));
							break;
						case Tags.GROWING_TIPS:
							activeFlower.setGrowingTips(new GrowingTips());
							break;
						case Tags.TEMPERATURE:
							String temperatureType = startElement.getAttributeByName(QName.valueOf(Tags.MEASURE))
									.getValue();
							xmlEvent = xmlEventReader.nextEvent();
							elementData = xmlEvent.asCharacters().getData();
							activeFlower.getGrowingTips().setTemperatureType(temperatureType);
							activeFlower.getGrowingTips().setTemperatureValue(Integer.parseInt(elementData));
							break;
						case Tags.LIGHTING:
							String lightRequiring = startElement.getAttributeByName(QName.valueOf(Tags.LIGHT_REQUIRING))
									.getValue();
							activeFlower.getGrowingTips().setLightRequiring(lightRequiring);
							break;
						case Tags.WATERING:
							String wateringMeasure = startElement.getAttributeByName(QName.valueOf(Tags.MEASURE))
									.getValue();
							xmlEvent = xmlEventReader.nextEvent();
							elementData = xmlEvent.asCharacters().getData();
							activeFlower.getGrowingTips().setWateringMeasure(wateringMeasure);
							activeFlower.getGrowingTips().setWateringValue(Integer.parseInt(elementData));
							break;
						case Tags.MULTIPLYING:
							xmlEvent = xmlEventReader.nextEvent();
							elementData = xmlEvent.asCharacters().getData();
							activeFlower.setMultiplying(elementData);
							break;
					}

				} else if (xmlEvent.isEndElement()) {
					EndElement endElement = xmlEvent.asEndElement();

					if (endElement.getName().getLocalPart().equals(Tags.FLOWER)) {
						flowerList.add(activeFlower);
					}
				}
			}
		} catch (XMLStreamException | FileNotFoundException e) {
			e.printStackTrace();
		}

		return flowerList;
	}

}