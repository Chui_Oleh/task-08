package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entity.Flower;
import com.epam.rd.java.basic.task8.entity.GrowingTips;
import com.epam.rd.java.basic.task8.entity.VisualParameters;
import com.epam.rd.java.basic.task8.xmlTags.Tags;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;


import java.util.ArrayList;
import java.util.List;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {

	private final String xmlFileName;

	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	private List<Flower> flowerList;
	private Flower activeFlower;
	private StringBuilder elementValue;

	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
		if (elementValue == null) {
			elementValue = new StringBuilder();
		} else {
			elementValue.append(ch, start, length);
		}
	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		switch (qName) {
			case Tags.FLOWERS:
				flowerList = new ArrayList<>();
				break;
			case Tags.FLOWER:
				activeFlower = new Flower();
				break;
			case Tags.VISUAL_PARAMETERS:
				activeFlower.setVisualParameters(new VisualParameters());
				break;
			case Tags.GROWING_TIPS:
				activeFlower.setGrowingTips(new GrowingTips());
				break;
			case Tags.AVE_LEN_FLOWER:
				activeFlower.getVisualParameters().setAveLenFlowerType(attributes.getValue(Tags.MEASURE));
				elementValue = new StringBuilder();
				break;
			case Tags.TEMPERATURE:
				activeFlower.getGrowingTips().setTemperatureType(attributes.getValue(Tags.MEASURE));
				elementValue = new StringBuilder();
				break;
			case Tags.LIGHTING:
				activeFlower.getGrowingTips().setLightRequiring(attributes.getValue(Tags.LIGHT_REQUIRING));
				elementValue = new StringBuilder();
				break;
			case Tags.WATERING:
				activeFlower.getGrowingTips().setWateringMeasure(attributes.getValue(Tags.MEASURE));
				elementValue = new StringBuilder();
				break;
			default:
				elementValue = new StringBuilder();
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		switch (qName) {
			case Tags.FLOWER:
				flowerList.add(activeFlower);
				activeFlower = null;
				break;
			case Tags.NAME:
				activeFlower.setName(elementValue.toString());
				break;
			case Tags.SOIL:
				activeFlower.setSoil(elementValue.toString());
				break;
			case Tags.ORIGIN:
				activeFlower.setOrigin(elementValue.toString());
				break;
			case Tags.STEM_COLOUR:
				activeFlower.getVisualParameters().setStemColour(elementValue.toString());
				break;
			case Tags.LEAF_COLOUR:
				activeFlower.getVisualParameters().setLeafColour(elementValue.toString());
				break;
			case Tags.AVE_LEN_FLOWER:
				activeFlower.getVisualParameters().setAveLenFlowerValue(Integer.parseInt(elementValue.toString()));
				break;
			case Tags.TEMPERATURE:
				activeFlower.getGrowingTips().setTemperatureValue(Integer.parseInt(elementValue.toString()));
				break;
			case Tags.WATERING:
				activeFlower.getGrowingTips().setWateringValue(Integer.parseInt(elementValue.toString()));
				break;
			case Tags.MULTIPLYING:
				activeFlower.setMultiplying(elementValue.toString());
				break;
		}
	}

	public String getXmlFileName() {
		return xmlFileName;
	}

	public List<Flower> getParsingResult() {
		return flowerList;
	}
}